const fs = require('fs');
const path = require('path');

module.exports.registerModels = function (keystone) {
    keystone.createList('Category', require('./Category.js'));
    keystone.createList('Lesson', require('./Lesson.js'));
    keystone.createList('User', require('./User.js'));
    let FileSchema = require('./File.js');
    keystone.createList('Sound', {...FileSchema({validation: {mimetype: 'audio'}}),});
    keystone.createList('Image', FileSchema({validation: {mimetype: 'image'}}));
    keystone.createList('Subtitle', FileSchema({validation: {mimetype: 'text'}}));
}