const {LocalFileAdapter} = require('@keystonejs/file-adapters');
const Fields = require('@keystonejs/fields');
const fs = require('fs');
const path = require('path');
const filestorage = process.env.FILE_STORAGE;
const storage_path = path.resolve(filestorage);
const {atTracking, byTracking} = require('@keystonejs/list-plugins');
const FileSize = require('filesize')

const fileAdapter = new LocalFileAdapter({
    src: storage_path,
    path: '/files',
});

module.exports = ({validation: {mimetype: validationMimeType = ''}}) => ({
    fields: {
        file: {
            type: Fields.File,
            isRequired: true,
            adapter: fileAdapter,
            hooks: {
                validateInput: ({
                                    operation,
                                    existingItem,
                                    originalInput,
                                    resolvedData,
                                    context,
                                    addFieldValidationError,
                                    listKey,
                                    fieldPath, // exists only for field hooks
                                }) => {
                    // Throw error objects or register validation errors with addFieldValidationError(<String>)
                    // Return values ignored
                    console.log('validate input called with', resolvedData);
                    if (validationMimeType && !resolvedData.file.mimetype.startsWith(validationMimeType)) {
                        addFieldValidationError('delam mikhad!');
                        throw  new Error('Error!');
                    }
                },
                beforeChange: async ({existingItem}) => {
                    if (existingItem && existingItem.file) {
                        await fileAdapter.delete(existingItem.file);
                    }
                },
                validateDelete: ({
                                     operation,
                                     existingItem,
                                     context,
                                     addFieldValidationError,
                                     listKey,
                                     fieldPath, // exists only for field hooks
                                 }) => {
                    /* todo fix this , field delete show success but don't delete file,
                    // todo dont throe error when item is deleted not field
                     // Throw error objects or register validation errors with addFieldValidationError(<String>)
                     // Return values ignored
                     console.log('validateDelete called with', existingItem);
                     addFieldValidationError('not allowed, please delete the item instead.');
                     // throw new Error('not allowed, please delete the item instead.')*/
                }
            },
        },
        name: {
            type: Fields.Text,
            adminConfig: {isReadOnly: true,},
        },
        encoding: {
            type: Fields.Text,
            adminConfig: {isReadOnly: true,},
        },
        mimetype: {
            type: Fields.Text,
            adminConfig: {isReadOnly: true,},
        },
        url: {
            type: Fields.Virtual,
            resolver: (item, args, context) => {
                return '/files/' + item.file.filename
            },
            adminConfig: {
                isReadOnly: true,
            }
        },
        size: {
            type: Fields.Text,
            adminConfig: {
                isReadOnly: true,
            }
        }
    },
    hooks: {
        resolveInput: ({
                                 operation,
                                 existingItem,
                                 originalInput,
                                 resolvedData,
                                 context,
                                 listKey,
                                 fieldPath, // exists only for field hooks
                             }) => {
            // Input resolution logic. Object returned is used in place of `resolvedData`.
            console.log('resolve input ::: ', resolvedData, originalInput);
            const {originalFilename, mimetype, encoding} = resolvedData.file;
            return new Promise(async (resolve, reject) => {
                try {
                    const {createReadStream} = await originalInput.file
                    const stream = createReadStream();
                    let file_size = 0;
                    stream.on('data', function (chunk) {
                        file_size += chunk.length;
                    }).on('end', () => {
                        const size = FileSize(file_size);
                        resolvedData = {...resolvedData,size, name: originalFilename, mimetype, encoding};
                        resolve(resolvedData);
                    })
                } catch (e) {
                    console.error(e);
                    resolvedData = {...resolvedData, name: originalFilename, mimetype, encoding};
                    resolve(resolvedData);
                }
            })
        },
        afterDelete: async ({existingItem}) => {
            if (existingItem.file) {
                await fileAdapter.delete(existingItem.file);
            }
        },
    },
    labelField: 'name',
    plugins: [
        byTracking(),
        atTracking(),
    ]
});

